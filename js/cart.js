document.addEventListener('DOMContentLoaded', function () {
    const cartBody = document.querySelector(".cart-body");
    const cart = {};

    window.addEventListener('click', function (e) {

        if (e.target.hasAttribute("data-cart")) {
            const card = e.target.closest(".product-card");
            // Переменная в которой храним данные о товаре
            const productInfo = {
                id: card.dataset.id,
                imgSrc: card.querySelector('.image-js').getAttribute('src'),
                title: card.querySelector('.title-js').innerText,
                description: card.querySelector('.description-js').innerText,
                price: card.querySelector('.price-js').innerText,
                counter: card.querySelector('[data-counter]').innerText
            };

            // localStorage.setItem('cart', JSON.stringify(productInfo));
            // const localCart = localStorage.getItem('cart');


            // Если товар уже в корзине
            const itemInCart = cartBody.querySelector(`[data-id="${productInfo.id}"]`);

            if (itemInCart) {
                countEl = itemInCart.querySelector('[data-counter]');
                countEl.innerText = parseInt(countEl.innerText) + parseInt(productInfo.counter);
            } else {
                const cartItemHTML =
                    `<div class="item-in-cart">
                    <div class="product-card cart-card" data-id="${productInfo.id}">
                        <div class="container cart-items">

                            <div class="cart-image">
                                <img src="${productInfo.imgSrc}"/>
                            </div>

                            <div class="cart-name">
                                <h4	class="product-item__title"> ${productInfo.title}
                                </h4>
                                <h5	class="product-item__description"> ${productInfo.description}
                                </h5>
                            </div>

                            <div class="product-features__number"> 
                                <div  data-action="minus">-</div>
                                <div class="counter" data-counter>${productInfo.counter}</div>
                                <div  data-action="plus">+</div>
                            </div>

                            <div>             
                                <span	class="product-features__price price-js"> ${productInfo.price} </span>
                            </div>

                            

                        </div>
                    </div>
                </div>`

                cartBody.insertAdjacentHTML('beforeend', cartItemHTML);
            }
            // Сбрасываем счетчик если добавили товар
            card.querySelector(["[data-counter]"]).innerText = "1";

            cartStatus();
        }
    });

    function cartStatus() {
        const cartTotal = document.querySelector(".cart-total");
        const cartOrderTitle = document.querySelector(".cart-order-title");
        const cartEmpty = document.querySelector("[data-cart-empty]");
        const orderForm = document.querySelector(".order-form");
        const form = document.querySelector(".form");

        // Есть товары    
        if (cartBody.querySelectorAll('.item-in-cart').length > 0) {
            cartEmpty.classList.add('none');
            cartOrderTitle.classList.remove('none');
            cartTotal.classList.remove('none');
            orderForm.classList.remove('none');
            form.classList.remove('none');
        }
        // Если корзина ПУСТА
        else {
            cartEmpty.classList.remove('none');
            cartOrderTitle.classList.add('none');
            cartTotal.classList.add('none');
            orderForm.classList.add('none');
            form.classList.add('none');
        }
        // Стоимость всего заказа
        let totalPrice = 0;
        cartBody.querySelectorAll(".item-in-cart").forEach(function (item) {

            const cartCounter = item.querySelector("[data-counter]").innerText;
            const priceOneItem = item.querySelector(".price-js").innerText;
            const price = parseInt(cartCounter) * parseInt(priceOneItem);

            totalPrice = totalPrice + price;
        });
        cartTotal.querySelector(".total-price").innerText = totalPrice;
    };
}, false);