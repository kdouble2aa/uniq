window.onload = function () {
  const form = document.querySelector('.form');
  const btn = form.querySelector('.btn');
  const name = form.querySelector('.name');
  const surname = form.querySelector('.surname');
  const fields = form.querySelectorAll('.field');
  let generateError = function (text) {
    let error = document.createElement('div');
    error.className = 'error';
    // error.style.color = 'red';
    error.innerHTML = text;
    return error;
  };
  let removeError = function () {
    let errors = form.querySelectorAll('.error');
    for (var i = 0; i < errors.length; i++) {
      errors[i].remove();
    }
  };
  const checkFields = function () {
    for (let i = 0; i < fields.length; i++) {
      if (!fields[i].value) {
        const error = generateError("Поле должно быть заполнено");
        form[i].parentElement.insertBefore(error, fields[i]);
      } else {
        const modal = document.getElementById("modal");
        const btn = document.getElementById("modalBtn");
        const span = document.getElementsByClassName("close")[0];
        btn.onclick = function () {
          modal.style.display = "block";
        }
        span.onclick = function () {
          modal.style.display = "none";
          location.reload();
        }
        window.onclick = function (event) {
          if (event.target == modal) {
            modal.style.display = "none";
            location.reload();
          }
        };
      };
    }
  };


  form.addEventListener('submit', function (event) {
    event.preventDefault();
    removeError();
    checkFields();
    // location.reload();
  });
};