document.addEventListener('DOMContentLoaded', function () {
  document.addEventListener('click', function (event) {
    if (event.target.hasAttribute("data-action")) {
      const counterCard = event.target.closest(".product-card");
      const counter = counterCard.querySelector("[data-counter]");
      if (event.target.dataset.action === 'plus') {

        counter.innerText = ++counter.innerText;
        if (event.target.closest('.cart-body')) {
          cartStatus();
        }


      } else if (event.target.dataset.action === 'minus') {
        if (event.target.closest(".cart-body")) {
          if (parseInt(counter.innerText) > 1) {
            counter.innerText = --counter.innerText;
          } else {
            event.target.closest(".item-in-cart").remove();
          }
          cartStatus();


        } else {
          if (parseInt(counter.innerText) > 1) {
            counter.innerText = --counter.innerText;
          }
        }

      }
    }
  });
}, false);