document.addEventListener('DOMContentLoaded', function () {
    const img = document.querySelectorAll(".image-js");

    for (let i = 0; i < img.length; i++) {
        img[i].addEventListener('mousemove', (e) => {
            const x = e.pageX - e.target.offsetLeft;
            const y = e.pageY - e.target.offsetTop;
            console.log(x, y);
            img[i].style.transformOrigin = `${x}px ${y}px`;
            img[i].style.transform = "scale(1.8)";
        });
        img[i].addEventListener('mouseleave', (e) => {

            img[i].style.transform = "scale(0.8)";
            img[i].style.transformOrigin = "center";

        });
    };
}, false);